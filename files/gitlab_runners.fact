#!/usr/bin/env python3

import subprocess
import re
import json
import toml

regex_token = re.compile(r"=([a-zA-Z0-9-_]+)")
regex_status = re.compile(r"[.][.][.] (is )?([a-z]+)")


output = subprocess.run(
    ["gitlab-runner", "verify", "--delete"],
    stdout=subprocess.PIPE,
    stderr=subprocess.STDOUT,
).stdout
output = output.splitlines()

config = toml.load("/etc/gitlab-runner/config.toml")

if len(output) < 3 or "runners" not in config:
    print(json.dumps([]))
    quit()

output = output[3:]
runners = []

for runner in config["runners"]:
    runners.append(
        {
            "name": runner["name"],
            "url": runner["url"],
            "auth_token": runner["token"],
            "registered": False,
        }
    )

for line in output:
    match_tok = regex_token.search(line.decode("utf8"))
    match_status = regex_status.search(line.decode("utf8"))

    runner = next(
        (
            r
            for r in runners
            if (
                r["auth_token"][5:14]
                if r["auth_token"].startswith("glrt-")
                else r["auth_token"][:8]
            )
            == match_tok.group(1)
        ),
        None,
    )
    if runner is None:
        continue
    runner["registered"] = True
    runner["status"] = match_status.group(2)

print(json.dumps(runners))
